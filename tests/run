#!/bin/sh
# -*- coding: utf-8 -*-
# Copyright (C) 2010, 2011, 2014-2016, 2018, 2019 Laboratoire de Recherche
# et Developpement de l'EPITA (LRDE).
# Copyright (C) 2003, 2004 Laboratoire d'Informatique de Paris 6
# (LIP6), département Systèmes Répartis Coopératifs (SRC), Université
# Pierre et Marie Curie.
#
# This file is part of Spot, a model checking library.
#
# Spot is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Spot is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Darwin needs some help in figuring out where non-installed libtool
# libraries are (on this platform libtool encodes the expected final
# path of dependent libraries in each library).
modpath='/Users/ploy/Desktop/spot-2.8.1-bucket/python/.libs:/Users/ploy/Desktop/spot-2.8.1-bucket/python/spot/.libs'
modpath=$modpath:'/Users/ploy/Desktop/spot-2.8.1-bucket/spot/ltsmin/.libs'
modpath=$modpath:'/Users/ploy/Desktop/spot-2.8.1-bucket/spot/.libs'
modpath=$modpath:'/Users/ploy/Desktop/spot-2.8.1-bucket/buddy/src/.libs'

# We need access to both the *.py files, and the *.so.  We used to
# rely on a module called ltihooks.py to teach the import function how
# to load a Libtool library, but it started to cause issues with
# Python 2.6.
pypath=
for i in python python/spot; do
    for j in '/Users/ploy/Desktop/spot-2.8.1-bucket' '/Users/ploy/Desktop/spot-2.8.1-bucket'; do
	pypath=$j/$i:$j/$i/.libs:$pypath
    done
done
pypath=${pypath%:}

PATH="/Users/ploy/Desktop/spot-2.8.1-bucket/bin:$PATH"
export PATH

test -z "$1" &&
    PYTHONPATH=$pypath DYLD_LIBRARY_PATH=$modpath exec $PREFIXCMD /Library/Frameworks/Python.framework/Versions/3.7/bin/python3
srcdir="."

# Reset CFLAGS, because some tests call the compiler, and we do not
# want to inherit parameters likes -std=c11 -fvisibility=hidden
CFLAGS=
export CFLAGS
CXX='g++'
export CXX
CXXFLAGS=' -fvisibility=hidden -fvisibility-inlines-hidden -DSPOT_BUILD -std=c++14 -g -O3 -ffast-math -fstrict-aliasing -fomit-frame-pointer'
export CXXFLAGS
top_builddir='/Users/ploy/Desktop/spot-2.8.1-bucket'
export top_builddir
top_srcdir='/Users/ploy/Desktop/spot-2.8.1-bucket'
export top_srcdir

SPOT_UNINSTALLED=1
export SPOT_UNINSTALLED

case $1 in
    */*)
	dir=${1%/*}
	dir=${dir##*/}
	mkdir -p $dir
	cd $dir
	case $1 in
	    ../*)
		base=../$1
		;;
	    [\\/$]* | ?:[\\/]* )
		base=$1
		;;
	    *)
		base=${1##*/}
		;;
	esac

	test $srcdir != '.' && srcdir="/Users/ploy/Desktop/spot-2.8.1-bucket/tests/$dir"
        shift
        set x "$base" "$@"
	shift
	;;
esac

export srcdir

case $1 in
  *.ipynb)
    PYTHONPATH=$pypath DYLD_LIBRARY_PATH=$modpath \
    PYTHONIOENCODING=utf-8:surrogateescape \
    exec $PREFIXCMD /Library/Frameworks/Python.framework/Versions/3.7/bin/python3 /Users/ploy/Desktop/spot-2.8.1-bucket/tests/python/ipnbdoctest.py "$@";;
  *.py)
    PYTHONPATH=$pypath DYLD_LIBRARY_PATH=$modpath \
    exec $PREFIXCMD /Library/Frameworks/Python.framework/Versions/3.7/bin/python3 "$@";;
  *.test)
    exec sh -x "$@";;
  *.pl)
    exec perl "$@";;
  *python*|*jupyter*)
    PYTHONPATH=$pypath DYLD_LIBRARY_PATH=$modpath \
    exec $PREFIXCMD "$@";;
  *)
    echo "Unknown extension" >&2
    exit 2;;
esac
